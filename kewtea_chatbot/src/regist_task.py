import requests
import json
import task_registration_form

def regist_task(customer, text, key):
    data = task_registration_form.data(customer)
    headers = {
        'Accept': 'application/json, text/plain, */*',
        'Authorization': "Bearer " + key
    }

    url = "https://admin.kchloe.co/secured/tasks"

    data.make_form(text)
    response = requests.post(url, json=data.json_data, headers=headers)

