import json
import requests
import pandas as pd
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer
import process
import datetime

class Summarize:
    def __init__(self, user_request, token):
        self.user_request = user_request
        self.token = token
        self.headers = {
            'Accept': 'application/json, text/plain, */*',
            'authorization': f'Bearer {self.token}'
        }

        # Initialization
        # self.main()

    def get_task_data(self):
        response = requests.get('https://admin.kchloe.co/secured/tasks?searchType=KEYWORD&size=500', headers=self.headers)
        tasks_json = json.loads(response.content)
        return tasks_json

    def task_list(self, data):
        task_code_data = []
        task_name_data = {}
        manager_data = {}
        task_details = {}
        for parent in data["contents"]:
            if parent['manager']['nickName'] is None:
                parent['manager']['nickName'] = 'N/A'
            task_details[parent['taskCode']] = {
                'taskName': parent['taskName'],
                'manager': parent['manager'].get("nickName"),
                'isParent': True,
                'children': []
            }
            task_code_data.append(parent["taskCode"])
            str_parent = process.preprocessed_text(parent["taskName"])
            task_name_data[parent["taskCode"]] = ' '.join(str_parent)
            manager_data[parent["taskCode"]] = parent["manager"].get("nickName")
            if parent["children"] != []:
                for child in parent['children']:
                    if child['manager']['nickName'] is None:
                        child['manager']['nickName'] = 'N/A'
                    child_details = {
                        'taskName': f"{child['taskName']}",
                        'manager': child['manager'].get("nickName"),
                        'isParent': False
                    }
                    task_details[child['taskCode']] = child_details
                    task_details[parent['taskCode']]['children'].append(child_details)
                    # raw_df.append(child_data)
                    task_code_data.append(child["taskCode"])
                    str_child = process.preprocessed_text(f'{child["taskName"]} {parent["taskName"]}')
                    task_name_data[child['taskCode']] = ' '.join(str_child)
                    manager_data[child["taskCode"]] = child["manager"].get("nickName")
            # data_df = pd.DataFrame(raw_df)
        return (task_code_data, task_name_data, manager_data, task_details)

    def extract_task(self):
        max_score, second_score = 0, 0
        max_code, second_code = None, None

        # Check Task Name
        name_vectorizer = TfidfVectorizer()
        name_vectorizer.fit(self.task_names)
        user_name_vec = name_vectorizer.transform([self.user_request])
        task_name_vec = name_vectorizer.transform(self.task_names)

        name_similarity = cosine_similarity(user_name_vec, task_name_vec)[0]

        # Check Manager Name
        manager_vectorizer = TfidfVectorizer()
        manager_vectorizer.fit(self.manager_names)
        user_man_vec = manager_vectorizer.transform([self.user_request])
        manager_name_vec = manager_vectorizer.transform(self.manager_names)

        manager_similarity = cosine_similarity(user_man_vec, manager_name_vec)[0]
        for i in range(len(self.task_codes)):
            # Check for Task Code
            has_code = False
            if self.task_codes[i] in self.tokenized_request:
                has_code = True

            # Calculate score
            task_name_sim = name_similarity[i]
            task_manager_sim = manager_similarity[i]
            score = task_name_sim + task_manager_sim * 0.3 + has_code

            if score > 0 and score > max_score:
                second_score = max_score
                second_code = max_code
                max_score = score
                max_code = self.task_codes[i]

        if max_score < 0.4:
            return None
        else:
            return {max_code: self.task_data[3][max_code]}

    def revision_differences(self, task_code):
        history_req = requests.get(f'https://admin.kchloe.co/secured/tasks/{task_code}/history', headers=self.headers)
        history = json.loads(history_req.content)
        if len(history) == 1:
            return None
        revision_num = history[0]['revisionNumber']
        prev_num = history[1]['revisionNumber']
        # Find content of revisions
        curr_req = requests.get(f'https://admin.kchloe.co/secured/tasks/{task_code}/history/{revision_num}', headers=self.headers).content
        prev_req = requests.get(f'https://admin.kchloe.co/secured/tasks/{task_code}/history/{prev_num}', headers=self.headers).content
        curr_version = json.loads(curr_req)
        prev_version = json.loads(prev_req)

        return process.compare_json(prev_version, curr_version)

    def summarize(self, task_info):
        summary = []
        if task_info is None or len(task_info) == 0:
            summary.append('기록된 변경사항이 없습니다.')
            return summary
        # 마감일 변경
        if 'deadlineTimestamp' in task_info:
            if task_info['deadlineTimestamp'][0] == None:
                new_date = datetime.datetime.fromtimestamp(task_info['deadlineTimestamp'][1]/1000)
                formatted_date = new_date.strftime('%Y-%m-%d')
                summary.append(f'요청 기한은 {formatted_date}으로 설정되었습니다.')
            elif task_info['deadlineTimestamp'][1] == None:
                summary.append(f'요청 기한은 삭제되었습니다.')
            else:
                curr_date = datetime.datetime.fromtimestamp(task_info['deadlineTimestamp'][0]/1000)
                new_date = datetime.datetime.fromtimestamp(task_info['deadlineTimestamp'][1]/1000)
                curr_format = curr_date.strftime('%Y-%m-%d')
                new_format = new_date.strftime('%Y-%m-%d')
                summary.append(f'요청기한이 {curr_format}에서 {new_format}으로 변경되었습니다.')
        # 완료일 변경
        if 'completedTimestamp' in task_info:
            if task_info['completedTimestamp'][0] == None:
                new_date = datetime.datetime.fromtimestamp(task_info['completedTimestamp'][1]/1000)
                formatted_date = new_date.strftime('%Y-%m-%d')
                summary.append(f'완료 날짜는 {formatted_date}으로 설정되었습니다.')
            elif task_info['completedTimestamp'][1] == None:
                summary.append(f'완료 날짜가 삭제되었습니다.')
            else:
                curr_date = datetime.datetime.fromtimestamp(task_info['completedTimestamp'][0]/1000)
                new_date = datetime.datetime.fromtimestamp(task_info['completedTimestamp'][1]/1000)
                curr_format = curr_date.strftime('%Y-%m-%d')
                new_format = new_date.strftime('%Y-%m-%d')
                summary.append(f'완료날짜가 {curr_format}에서 {new_format}으로 변경되었습니다.')
        # 담당자 변경
        if 'manager' in task_info:
            if task_info['manager'][0] == None:
                summary.append(f'담당자가 {task_info["manager"][1]}님으로 변경되었습니다.')
            elif task_info['manager'][1] == None:
                summary.append(f'담당자가 없음으로 변경되었습니다.')
            else:
                summary.append(f'담당자가 {task_info["manager"][0]}에서 {task_info["manager"][1]}으로 변경되었습니다.')
        # 상태값 변경
        if 'status' in task_info:
            if task_info['status'][0] == None:
                summary.append(f'현재 작업상태가 {task_info["status"][1]}으로 변경되었습니다.')
            elif task_info['status'][1] == None:
                summary.append(f'현재 작업상태가 없음으로 변경되었습니다.')
            else:
                summary.append(f'현재 작업상태가 {task_info["status"][0]}에서 {task_info["status"][1]}으로 변경되었습니다.')
        return summary

    def recent_comment(self, task_code):
        comm_check = json.loads(requests.get(f'https://admin.kchloe.co/secured/tasks/{task_code}/comments', headers=self.headers).content.decode('utf-8'))
        if len(comm_check) == 0:
            return None
        return comm_check[0]['bodyText']

    def main(self):
        self.tokenized_request = process.preprocessed_text(self.user_request)
        raw_data = self.get_task_data()
        self.task_data = self.task_list(raw_data)
        self.task_codes = [code for code in self.task_data[0]]
        self.task_names = [name for name in self.task_data[1].values()]
        self.manager_names = [name for name in self.task_data[2].values()]


        extracted_task = self.extract_task() # {taskCode: task_details}
        if extracted_task is None:
            return "죄송합니다. 해당 작업을 못찾았습니다. 어떤 작업인지 조금 더 구체적으로 말씀해주실 수 있나요?"
        else:
            task_code = list(extracted_task.keys())[0]
            differences = self.revision_differences(task_code) # {category: [prev_val, curr_val]}
            output_str = f'''{self.task_data[3][task_code]['taskName']} 작업의 변동사항을 요약해드리겠습니다'''
            summary = self.summarize(differences)
            for detail in summary:
                output_str = output_str + '\n' + detail
            comments = self.recent_comment(task_code)
            if comments is not None:
                output_str = output_str + '\n' + f'최근 채팅내용은 다음과 같습니다: {comments}'
        return output_str
    
# Execute Program
# headers = {
#     'Content-Type': 'application/json;charset=UTF-8'
# }
# data = {
#     'username': 'jonghyeok19@kewtea.com',
#     'password': 'Peter0109!'
# }
# url = 'https://admin.kchloe.co/signin'
# response = requests.post(url, headers=headers, json=data)

# token = response.content.decode('utf-8')

# test = Summarize("작업분석기 현황 알려줘", token)
# print(test.main())
