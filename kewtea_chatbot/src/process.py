import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords

nltk.download('punkt')
nltk.download('stopwords')

def detect_lang(text):
    if len(text) <= 0:
        return 'other'
    extract = text.strip().replace('?','').replace('!','').replace('.','').replace(',','').split()
    en, ko, other = 0, 0, 0
    for w in extract:
        if len(w) > 0:
            for c in w:
                code_point = ord(c)
                if (0x0041 <= code_point <=0x005A) or (0x0061 <= code_point <= 0x007A):
                    en += 1
                elif (0xAC00 <= code_point <= 0xD7AF) or (0x1100 <= code_point <= 0x11FF) or (0x3130 <= code_point <= 0x318F):
                    ko += 1
                else:
                    other += 1
    if en == max(en, ko, other):
        return 'en'
    elif ko == max(en, ko, other):
        return 'ko'
    else:
        return 'other'


def preprocessed_text(text):
    if detect_lang(text) == 'ko':
        processed_text = text.strip().replace('?','').replace('!','').replace('.',' ').replace(',',' ').replace('님 ',' ').replace('씨 ', ' ').replace('는 ',' ').replace('은 ',' ').split()
        stop_words = set(['을', '를', '이', '가', '은', '는', '및', '됐나요', '드립니다'])
    elif detect_lang(text) == 'en':
        text = text.replace('?','').replace('!','').replace('.','').replace(',','').replace('(',' ').replace(')',' ')
        processed_text = word_tokenize(text)
        stop_words = set(stopwords.words('english'))
    else:
        return None
    tokens = [word for word in processed_text if word not in stop_words]
    return tokens

def compare_json(obj1, obj2):
    differences = {}
    if obj1 == obj2:
        return differences
    for key in obj1:
        if key not in obj2 or obj1[key] != obj2[key]:
            differences[key] = [obj1[key], obj2[key]]
    for key in obj2:
        if key not in obj1:
            differences[key] = [None, obj2[key]]
    return differences