#이 파일이 main역할을 함.
import pickle
import make_IsWf_model
import make_workclf_model
import regist_task
import summary
import requests

#konlpy 의존성 설치 - jpype1
#https://github.com/jpype-project/jpype/releases

PATH_workclf = "./models/workclf_model"
PATH_IsWfmodel = "./models/IsWf_model"

test_text = "보름까지 영수증 정리 좀 해줘."
test_customer = "namyoung.cho@kewtea.com"

# IsfWfmodel 가지고 오는 함수
def get_IsfWfmodel():
    with open(PATH_IsWfmodel, 'rb') as f:
        loaded_model = pickle.load(f)

        return loaded_model
    
#workclfmodel가지고 오는 함수
def get_workclfmodel():
    with open(PATH_workclf, 'rb') as f:
        loaded_model = pickle.load(f)

        return loaded_model

#등록을 위해 웹에서 토큰받기
def make_key():
    url = "https://admin.kchloe.co/signin"

    headers = {
        "Content-Type": "application/json;charset=UTF-8"
    }

    data = {
        "username": "namyoung.cho@kewtea.com",
        "password": "key329311!"
    }

    response = requests.post(url, json=data, headers=headers)
    a = response.content.decode("ascii")  # 응답 데이터 확인
    print(a)

    return a

def chatparse(customer, text):
    #두 개의 모델 가지고 옴
    IsWfmodel = get_IsfWfmodel()
    workclfmodel = get_workclfmodel()

    #두 모델의 word_index에 대한 토큰
    token_IsWf = make_IsWf_model.tokenize_text(text)
    token_workclf = make_workclf_model.tokenize_text(text)

    #작업인지 아닌지 분류하는 if문
    if IsWfmodel.predict(token_IsWf)[0]==1:
        print("this is wf")
        key = make_key()

        #요약 등록 분류 else문이 등록임.
        if workclfmodel.predict(token_workclf)[0]==1:
            summ = summary.Summary(test_text, key)
            print(summ.main())
        else:
            regist_task.regist_task(customer, text, key)

    else:
        print("not wf")






