import re
from datetime import datetime
from konlpy.tag import Okt
from datetime import datetime, timedelta

#받아야 할 input:{client}, {task text}
#진입 시 각 요소로 초기화, 전체 내용을 변경 하고싶으면 make_form()사용하면 됨.

class data:
  def __init__(self, customer):
    self.taskName = ""
    self.description = ""
    self.client = customer
    self.manager = customer
    self.status = "ACTIVATED"
    self.quality = 0
    self.deadlineTimestamp = int(datetime.today().timestamp() * 1000)
    self.workflowNo = 21
    self.projectCode = None
    self.estimatedCost = "0"
    self.estimatedTime = "0"
    self.tags = ["chatbot"]


  def make_taskName(self, text):
    
    okt = Okt()
    tokens = okt.pos(text)
    reversed_tokens = list(reversed(tokens))

    #거꾸로된 배열에서 첫 번째로 나오는 동사찾기
    for token in reversed_tokens:
      if token[1]=='Verb':
        verb = token[0]
        break
    
    #뒤에서 첫 번째 명사 찾기
    count = 0
    for token in reversed_tokens:
      if token[1]=='Noun':
        if reversed_tokens[count+1][1]=='Josa':
          reversed_tokens.pop(count+1)
        break
      count +=1

    #명사 아닐때 까지 명사 넣기
    nouns = []
    for token in reversed_tokens[count:]:
      if token[1]!='Noun' and token[1]!='Alpha' and token[1]!='Modifier':
        break
      nouns.insert(0,token[0])

    #텍스트로 합치기
    result = ""
    for noun in nouns:
      result = result + " " + noun
    
    result = result + verb

    self.taskName = "chatbot: " + result
    return result

  ###
  #매니저 탐색함수 
  #이름까지는 분리 완료 하지만 매니저 닉네임까지는 탐색이 힘들음. 방법 필요
  def seperate_manager(self, text):
    #text 분리시켜서 넣기
    okt = Okt()
    words = okt.morphs(text)
    keywords = ["씨", "님", "께서", "군", "이", "옹", "에게", "니"]
    manager = None

    for keyword in keywords:
        if keyword in words:
            keyword_index = words.index(keyword)
            if keyword_index > 0:
                word_before_keyword = words[keyword_index - 1]
                manager = word_before_keyword
                self.manager = manager
                break

    return manager


#날짜 추출하는 함수
  def seperate_date(self, text):
    okt = Okt()
    words = okt.morphs(text)
    deadline = 0

    #년 월 일 잡는 부분
    day_pattern = r'(\d{1,2}일)' 
    month_pattern = r'(\d{1,2}월)'
    year_pattern = r'(\d{4}년)'

    day_match = re.search(day_pattern, text)
    month_match = re.search(month_pattern,text)
    year_match = re.search(year_pattern, text)

    #날짜 패턴이 맞다면 실행 조건은 day month year 순서
    #년,월,일의 default는 오늘날짜
    if year_match:
      date_str = year_match.group() + month_match.group() + day_match.group
      date_obj = datetime.strptime(date_str, '%Y년%m월%d일')
      deadline = date_obj.timestamp()
      self.deadlineTimestamp = int(deadline * 1000)

    else:
      if month_match:
        date_str = str(datetime.now().year) + "년" + month_match.group() + day_match.group()
        date_obj = datetime.strptime(date_str, '%Y년%m월%d일')
        deadline = date_obj.timestamp()
        self.deadlineTimestamp = int(deadline * 1000)

      else:
        if day_match:
          date_str = str(datetime.now().year) + "년" + str(datetime.now().month) +"월"+ day_match.group()
          date_obj = datetime.strptime(date_str, '%Y년%m월%d일')
          deadline = date_obj.timestamp()
          self.deadlineTimestamp = int(deadline * 1000)

    ######
    #한글 날짜 비교하는 부분
    #날짜 한국어 리스트, 추가사항 있으면 리스트에 추가 가능합니다..
    korean_words = [{"word": "오늘", "count": 0}, 
                    {"word": "내일", "count": 1},
                    {"word": "이틀", "count": 2},
                    {"word": "사흘", "count": 3}, 
                    {"word": "나흘", "count": 4}, 
                    {"word": "닷새", "count": 5}, 
                    {"word": "엿새", "count": 6}, 
                    {"word": "이레", "count": 7},
                    {"word": "여드레", "count": 8},
                    {"word": "아흐레", "count": 9},
                    {"word": "열흘", "count": 10},
                    {"word": "다음주", "count": 7},
                    {"word": "다음달", "count": 30},
                    {"word": "금일", "count": 0},
                    {"word": "명일", "count": 1},
                    {"word": "보름", "count": 15}]

    today = datetime.today()
    morphs = okt.morphs(text)

    #같은 단어가 있는지 비교하는 부분
    for korean_word in korean_words:

      #다음주, 다음달을 잡기위해 합치는 작업 시행.
      for morph in morphs:
        if morph in ["주", "달"]:
          temp_word = temp_word + morph

        else:
          temp_word = morph

        if korean_word["word"]==temp_word:
          date_obj = today + timedelta(days=korean_word["count"])
          deadline = date_obj.timestamp()
          self.deadlineTimestamp = int(deadline * 1000)
          print(date_obj)

          break

    return deadline

  #프로젝트 반환하는 함수, 프로젝트 코드를 가지고오지 못하는중.
  def seperate_projectCode(self, text):
    okt = Okt()
    words = okt.morphs(text)
    #탐색용 키워드
    keywords = ["프로젝트", "project", "일", "하", "하던", "하던거"]
    project = []

    for keyword in keywords:
      if keyword in words:
        keyword_index = words.index(keyword)
        if keyword_index > 0:
          word_before_keyword = words[keyword_index - 1]
          project.append(word_before_keyword)

    #있으면 self project[0]로 초기화
    if project is not None:
      self.projectCode = project[0]

    return project

  def make_form(self, text):
    self.seperate_date(text)
    self.make_taskName(text)

    self.json_data = {
        "taskName": self.taskName,
        "description": self.description,
        "client": self.client,
        "manager": self.manager,
        "status": self.status,
        "quality": self.quality,
        "deadlineTimestamp": self.deadlineTimestamp,
        "workflowNo": self.workflowNo,
        "projectCode": self.projectCode,
        "estimatedCost": self.estimatedCost,
        "estimatedTime": self.estimatedTime,
        "tags": self.tags
    }


