#이거는 모델 학습용 파일입니다. 새로운 데이터가 추가되면 실행시켜주세요.
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import LinearSVC
from sklearn.preprocessing import PolynomialFeatures
import matplotlib.pyplot as plt
import pickle


data = "./training_data/classifyIsWf_data.csv"
model_path = "./models/IsWf_model"

#dataframe 생성 x = df["text"], y = df["classify"]
def get_df(data):
    df = pd.read_csv(data, encoding = 'utf-8')
    return df

#model 생성하는 함수, polynomial svm 모델임 degree = 2
def get_model(train_x, train_y):
    polynomial_svm_clf = Pipeline([
        ("poly_features", PolynomialFeatures(degree = 2)),
        ("scaler", StandardScaler()),
        ("svm_clf", LinearSVC(C = 100, loss = "hinge")), #loss 기본값 힌지제곱
    ])
    model = polynomial_svm_clf.fit(train_x,train_y)

    return model

#데이터 전처리 함수 종속변수x와 반응변수y 반환
def preprocessing(df):
    # 토큰화 및 단어 사전 구축
    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(df["text"])

    # 빈도 1 이하인 단어 제거
    min_word_count = 0  # 최소 단어 빈도 설정
    filtered_words = [word for word, count in tokenizer.word_counts.items() if count >= min_word_count]
    tokenizer.word_index = {word: index for index, word in enumerate(filtered_words, start=1)}
    
    #단어사전
    word_index = tokenizer.word_index

    # 텍스트를 정수 시퀀스로 변환
    sequences = tokenizer.texts_to_sequences(df["text"])
    datas = sequences

    df_y = df["is_wf"]
    df_x = pad_sequences(datas, maxlen = len(word_index))

    #변수 나누기
    column_name = []
    for num in range(len(word_index)):
        name = "word" + str(num)
        column_name.append(name)

    #df_x pandas객체로 표시
    df_x_np = np.array(df_x)
    df_x_pd = pd.DataFrame(df_x_np, columns=column_name)

    return df_x_pd, df_y

def save_model(model):
    with open(model_path, 'wb') as f:
        pickle.dump(model, f)

def tokenize_text(text):
    df = get_df(data)
    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(df["text"])

    # 빈도 1 이하인 단어 제거
    min_word_count = 0  # 최소 단어 빈도 설정
    filtered_words = [word for word, count in tokenizer.word_counts.items() if count >= min_word_count]
    tokenizer.word_index = {word: index for index, word in enumerate(filtered_words, start=1)}

    #단어 사전
    word_index = tokenizer.word_index

    # 텍스트를 정수 시퀀스로 변환
    sequences = tokenizer.texts_to_sequences(df["text"])
    
    ####
    #받은 text 정수 index로 변환하는 부분 
    tokenizer.fit_on_texts(text)
    text_token = tokenizer.texts_to_sequences(text)

    tokens_list = []
    token_list = []

    for tokens in text_token:
        for token in tokens:
            token_list.append(token)
    tokens_list.append(token_list)

    result = pad_sequences(tokens_list, maxlen = len(word_index))

    return result

def main():
    df = get_df(data)

    x,y = preprocessing(df)

    #모델 가지고 옴 여러 모델을 사용하는 것이 아니기 때문에 test는 따로 진행하지 않았음. 
    fit_model = get_model(x,y)

    #모델 저장
    save_model(fit_model)

main()